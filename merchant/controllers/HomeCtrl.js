controllerModule.controller("HomeCtrl",["$scope",function($scope){
    $scope.homeList = {
		europe : [
			{
				imgUrl : "./images/europe/aodi.jpg",
				id : "",
				name : "奥迪"
			},{
				imgUrl : "./images/europe/baoma.jpg",
				id : "",
				name : "宝马"
			},{
				imgUrl : "./images/europe/baoshijie.jpg",
				id : "",
				name : "保时捷"
			},{
				imgUrl : "./images/europe/benchi.jpg",
				id : "",
				name : "奔驰"
			},{
				imgUrl : "./images/europe/biaozhi.jpg",
				id : "",
				name : "标志"
			},{
				imgUrl : "./images/europe/bieke.jpg",
				id : "",
				name : "别克"
			},{
				imgUrl : "./images/europe/daoqi.jpg",
				id : "",
				name : "道奇"
			},{
				imgUrl : "./images/europe/dazhong.jpg",
				id : "",
				name : "大众"
			},{
				imgUrl : "./images/europe/feiyate.jpg",
				id : "",
				name : "菲亚特"
			},{
				imgUrl : "./images/europe/fute.jpg",
				id : "",
				name : "福特"
			},{
				imgUrl : "./images/europe/jeep.jpg",
				id : "",
				name : "Jeep"
			},{
				imgUrl : "./images/europe/jiebao.jpg",
				id : "",
				name : "捷豹"
			},{
				imgUrl : "./images/europe/kaidilake.jpg",
				id : "",
				name : "凯迪拉克"
			},{
				imgUrl : "./images/europe/kelaisile.jpg",
				id : "",
				name : "克莱斯勒"
			},{
				imgUrl : "./images/europe/leinuo.jpg",
				id : "",
				name : "雷诺"
			},{
				imgUrl : "./images/europe/luhu.jpg",
				id : "",
				name : "路虎"
			},{
				imgUrl : "./images/europe/mini.jpg",
				id : "",
				name : "mini"
			},{
				imgUrl : "./images/europe/oubao.jpg",
				id : "",
				name : "欧宝"
			},{
				imgUrl : "./images/europe/sibalu.jpg",
				id : "",
				name : "斯巴鲁"
			},{
				imgUrl : "./images/europe/sikeda.jpg",
				id : "",
				name : "斯柯达"
			},{
				imgUrl : "./images/europe/woerwo.jpg",
				id : "",
				name : "沃尔沃"
			},{
				imgUrl : "./images/europe/xuefulan.jpg",
				id : "",
				name : "雪佛兰"
			},{
				imgUrl : "./images/europe/xuetielong.jpg",
				id : "",
				name : "雪铁龙"
			},
		],
		japan : [
			{
				imgUrl : "./images/japan/bentian.jpg",
				id : "",
				name : "本田"
			},{
				imgUrl : "./images/japan/fengtian.jpg",
				id : "",
				name : "丰田"
			},{
				imgUrl : "./images/japan/leikesasi.jpg",
				id : "",
				name : "雷克萨斯"
			},{
				imgUrl : "./images/japan/linmu.jpg",
				id : "",
				name : "铃木"
			},{
				imgUrl : "./images/japan/mazida.jpg",
				id : "",
				name : "马自达"
			},{
				imgUrl : "./images/japan/ouge.jpg",
				id : "",
				name : "讴歌"
			},{
				imgUrl : "./images/japan/qiya.jpg",
				id : "",
				name : "起亚"
			},{
				imgUrl : "./images/japan/richan.jpg",
				id : "",
				name : "日产"
			},{
				imgUrl : "./images/japan/sanlin.jpg",
				id : "",
				name : "三菱"
			},{
				imgUrl : "./images/japan/xiandai.jpg",
				id : "",
				name : "现代"
			},{
				imgUrl : "./images/japan/yinfeinidi.jpg",
				id : "",
				name : "英菲尼迪"
			}
		],
    	china : [{
				imgUrl : "./images/china/baojun.jpg",
				id : "",
				name : "宝骏"
			},{
				imgUrl : "./images/china/beiqi.jpg",
				id : "",
				name : "北汽"
			},{
				imgUrl : "./images/china/byd.jpg",
				id : "",
				name : "比亚迪"
			},{
				imgUrl : "./images/china/changan.jpg",
				id : "",
				name : "长安"
			},{
				imgUrl : "./images/china/changcheng.jpg",
				id : "",
				name : "长城"
			},{
				imgUrl : "./images/china/chuanqi.jpg",
				id : "",
				name : "传祺"
			},{
				imgUrl : "./images/china/dihao.jpg",
				id : "",
				name : "帝豪"
			},{
				imgUrl : "./images/china/dongfeng.jpg",
				id : "",
				name : "东风"
			},{
				imgUrl : "./images/china/dongnan.jpg",
				id : "",
				name : "东南"
			},{
				imgUrl : "./images/china/ds.jpg",
				id : "",
				name : "DS"
			},{
				imgUrl : "./images/china/guanzhi.jpg",
				id : "",
				name : "观致"
			},{
				imgUrl : "./images/china/hafu.jpg",
				id : "",
				name : "哈佛"
			},{
				imgUrl : "./images/china/haima.jpg",
				id : "",
				name : "海马"
			},{
				imgUrl : "./images/china/jianghuai.jpg",
				id : "",
				name : "江淮"
			},{
				imgUrl : "./images/china/jili.jpg",
				id : "",
				name : "吉利"
			},{
				imgUrl : "./images/china/mingjue.jpg",
				id : "",
				name : "名爵"
			},{
				imgUrl : "./images/china/nazhijie.jpg",
				id : "",
				name : "纳智捷"
			},{
				imgUrl : "./images/china/qichen.jpg",
				id : "",
				name : "启辰"
			},{
				imgUrl : "./images/china/qirui.jpg",
				id : "",
				name : "奇瑞"
			},{
				imgUrl : "./images/china/rongwei.jpg",
				id : "",
				name : "荣威"
			},{
				imgUrl : "./images/china/wulin.jpg",
				id : "",
				name : "五菱"
			},{
				imgUrl : "./images/china/xiali.jpg",
				id : "",
				name : "夏利"
			},{
				imgUrl : "./images/china/yiqi.jpg",
				id : "",
				name : "一汽"
			},{
				imgUrl : "./images/china/zhonghua.jpg",
				id : "",
				name : "中华"
			},{
				imgUrl : "./images/china/zhongtai.jpg",
				id : "",
				name : "众泰"
			}
		],
		danxiang : [
			{
				imgUrl : "./images/danxiang/chaichejian.jpg",
				id : "",
				name : "拆车件"
			},{
				imgUrl : "./images/danxiang/dengguangshengji.jpg",
				id : "",
				name : "灯光升级"
			},{
				imgUrl : "./images/danxiang/dianpenjian.jpg",
				id : "",
				name : "电喷件"
			},{
				imgUrl : "./images/danxiang/dipanxitong.jpg",
				id : "",
				name : "底盘系统"
			},{
				imgUrl : "./images/danxiang/fadianji.jpg",
				id : "",
				name : "发电机马达"
			},{
				imgUrl : "./images/danxiang/kongtiaosanre.jpg",
				id : "",
				name : "空调散热"
			},{
				imgUrl : "./images/danxiang/lunguluntai.jpg",
				id : "",
				name : "轮毂轮胎"
			},{
				imgUrl : "./images/danxiang/neishifanxin.jpg",
				id : "",
				name : "内饰翻新"
			},{
				imgUrl : "./images/danxiang/qibaogongju.jpg",
				id : "",
				name : "汽保工具"
			},{
				imgUrl : "./images/danxiang/qicheboli.jpg",
				id : "",
				name : "汽车玻璃"
			},{
				imgUrl : "./images/danxiang/qichegaizhuang.jpg",
				id : "",
				name : "汽车改装"
			},{
				imgUrl : "./images/danxiang/runhuayou.jpg",
				id : "",
				name : "润滑油"
			},{
				imgUrl : "./images/danxiang/xudianchi.jpg",
				id : "",
				name : "蓄电池"
			},{
				imgUrl : "./images/danxiang/yinyingdaohang.jpg",
				id : "",
				name : "影音导航"
			},{
				imgUrl : "./images/danxiang/yisunjian.jpg",
				id : "",
				name : "易损件"
			},{
				imgUrl : "./images/danxiang/zhulibang.jpg",
				id : "",
				name : "助力泵"
			}
		]
		
    }

	$scope.curSelect = $scope.homeList['china'];
	$scope.areaTabAct = 0;

	$scope.tabArea = function(type,index){
		$scope.curSelect = $scope.homeList[type];
		$scope.areaTabAct = index;
	}
	
}]);