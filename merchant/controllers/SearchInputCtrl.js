controllerModule.controller("SearchInputCtrl",["$scope","$location",function($scope,$location){
    $scope.searchKey = "";
    $scope.search = function(){
        if(!$scope.searchKey){
            return;
        }
        $location.path("/merchant/list").search({
            searchKey : $scope.searchKey
        })
    }
}]);