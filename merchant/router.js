merchantApp.config(['$routeProvider','$locationProvider',function($routeProvider,$locationProvider){
    //$locationProvider.html5Mode(true);
    $routeProvider.otherwise("/");
    $routeProvider
        .when("/",{
            templateUrl : "./tpls/home.html",
            controller : "HomeCtrl"
        })
        .when('/merchant/list',{
        	templateUrl : "./tpls/merchantListTpl.html",
        	controller : "MerchantListCtrl"
        })
        .when('/merchant/detail',{
        	templateUrl : "./tpls/merchantDetailTpl.html",
        	controller : "MerchantDetailCtrl"
        })
        .when('/merchant/searchInput',{
        	templateUrl : "./tpls/searchInput.html",
        	controller : "SearchInputCtrl"
        })
}])